#!/usr/bin/python3
# Script to set up environment in a new server or machine
import os
import sys


def main():
    """
    Creates two executable files in the new server/machine to run the program in two different modes.
    """
    platform_system = sys.platform
    py_path = find_pypath()
    t_file = "pipeline.py"
    extra_info_win = "/python.exe " + t_file + " %*\n"
    extra_info_lin = "/home/dipasa/shared/deps/Python-3.8.0/python " + t_file

    if platform_system == 'win32':  # We are in windows
        try:
            run_file = open("SUIT.bat", "r")
            file_data = []
            for line in run_file:
                file_data.append(line)
            run_file.close()
            str_comp = file_data[1][0:(len(file_data[1]) - len(extra_info_win))]
            if str_comp != py_path:
                print("No valid executable file found. Creating a new one...")
                exe_file_creation(platform_system, py_path, t_file)
                print("Done")

        except FileNotFoundError:
            print("No executable file found. Creating a new one...")
            exe_file_creation(platform_system, py_path, t_file)
            print("Done")

    elif platform_system == 'linux' or platform_system == 'linux2':  # We are in linux so we need a .sh file
        try:
            run_file = open("SUIT.sh", "r")
            file_data = []
            for line in run_file:
                file_data.append(line)
            run_file.close()
            str_comp = file_data[3]
            if str_comp != extra_info_lin:
                print("No valid executable file found. Creating a new one...")
                exe_file_creation(platform_system, py_path, t_file)
                print("Done")

        except FileNotFoundError:
            print("No executable file found. Creating a new one...")
            exe_file_creation(platform_system, py_path, t_file)
            print("Done")
        except IndexError:
            print("No valid executable file found. Creating a new one...")
            exe_file_creation(platform_system, py_path, t_file)
            print("Done")
    else:
        sys.exit("The operative system platform is not supported.")

    return


def find_pypath():
    # I need to find where is python located in the server
    py_dir = os.path.dirname(sys.executable)
    cur_path = os.getcwd()

    py_path = '.'
    jump_dir = ''
    add_dir = ''
    q = ''
    s = ''
    path_flag = False
    q_flag = False
    s_flag = False
    tot_div = False
    dir_flag = True
    jump_flag = True

    for i in range(max(len(py_dir), len(cur_path))):
        if not q_flag:
            try:
                q = py_dir[i]
            except IndexError:
                q_flag = True
                q = ''
        if not s_flag:
            try:
                s = cur_path[i]
            except IndexError:
                s_flag = True
                s = ''

        if not path_flag:
            if q == s:
                continue
            else:
                py_path = py_path + '/'
                path_flag = True  # Here we find where paths diverge

                if q != '' and s != '':
                    tot_div = True
                else:
                    tot_div = False
        else:  # We create now the string with the new path
            if not tot_div:
                if q != '' and s == '':
                    py_path = py_path + q
                elif q == '' and s != '':
                    if jump_flag:
                        py_path = py_path + '..'
                        jump_flag = False
                    else:
                        if s == '/':
                            py_path = py_path + '/'
                            jump_flag = True
            else:  # paths diverge and go deep in directories: q != '' and s != ''
                # First we check if we need to jump from the directory
                if jump_flag:
                    jump_dir = jump_dir + '..'
                    jump_flag = False
                else:
                    if s == '/':
                        jump_dir = jump_dir + '/'
                        jump_flag = True

                # Second we check if we need to dive into directories
                if q != '':
                    if dir_flag:  # we have to add the first letter of the first directory since it hasn't been added
                        add_dir = add_dir + py_dir[i - 1] + q
                        dir_flag = False
                    else:
                        add_dir = add_dir + q

    # If we are in a case of total divergence we have to add the jumps and the dives
    if tot_div:
        py_path = py_path + jump_dir + '/' + add_dir

    return py_path


def exe_file_creation(plat_sys, python_path, tool_file):
    """
    Creates two executable file depending on the operative system we are (Linux or Windows + Vagrant)
    :param plat_sys: Platform system in which the code is running
    :param python_path: Path of the python file
    :param tool_file: Name of the tool file THIS WILL DISAPPEAR WHEN A NAME IS DECIDED
    """
    if plat_sys == 'linux' or plat_sys == 'linux2':
        # Simulation mode file
        exe_file = open("SUIT.sh", "w")
        exe_file.write("#!/bin/tcsh\n\n")
        exe_file.write("setenv PYTHONPATH ${PYTHONPATH}:")
        exe_file.write('"/home/dipasa/SUIT/py/deps"')
        exe_file.write("\n\n./py/Python-3.8.0/python " + tool_file + " -r")
        exe_file.close()

        # Analysis mode file
        exe_file = open("SUIT_analysis.sh", "w")
        exe_file.write("#!/bin/tcsh\n\n")
        exe_file.write("setenv PYTHONPATH ${PYTHONPATH}:")
        exe_file.write('"/home/dipasa/SUIT/py/deps"')
        exe_file.write("\n\n./py/Python-3.8.0/python " + tool_file)
        exe_file.close()

        convert2unix("SUIT.sh")
        make_file_ex(plat_sys, "SUIT.sh")
        convert2unix("SUIT_analysis.sh")
        make_file_ex(plat_sys, "SUIT_analysis.sh")

    else:  # We are in Windows
        exe_file = open("SUIT.bat", "w")
        exe_file.write("@echo off\n")
        exe_file.write(python_path + "/python.exe" + " " + tool_file + " %*\n")
        exe_file.write("pause")
        exe_file.close()
    return


def convert2unix(file):
    os.system("dos2unix " + file)
    return


def make_file_ex(plat_sys, file):
    if plat_sys == "linux" or plat_sys == "linux2":
        os.system("chmod +x " + file)
    else:
        print("In windows, '.bat' files are already executable.")
    return


if __name__ == "__main__":
    main()
