import sys
import numpy as np
from create_sps import sourcesFromArgs
from sim_tools import get_file_name
from namespaces import suit_results as suit_res
from namespaces import suit_analysis as suit
from namespaces import source_characteristics as source_ch
from namespaces import sport_params


def create_result_dictionary():
    result_dict = {
        suit_res.source_name: [],
        suit_res.energy: [],
        suit_res.optics_name: [],
        suit_res.inclination: [],
        suit_res.azimuth: [],
        suit_res.cross_area_simulated: {
            suit_res.effective_area_total: [],
            suit_res.effective_area_focused: [],
            suit_res.effective_area_straylight: []
        },
        #suit_res.cross_area_analytical: {
        #    suit_res.effective_area_total: [],
        #    suit_res.effective_area_focused: [],
        #    suit_res.effective_area_straylight: []
        #},
        suit_res.half_energy_width: {
            suit_res.hew_x: [],
            suit_res.hew_x_units: [],
            suit_res.hew_y: [],
            suit_res.hew_y_units: []
        }
    }
    return result_dict


def append_result(result_dict, simulation_dictionary, effective_areas, half_energy_width):
    if simulation_dictionary[suit.source][source_ch.type] == sport_params.extended_source:
        fits_stc = sourcesFromArgs(sys.platform, simulation_dictionary[suit.source][source_ch.file_name])
        object_name = fits_stc.object
    else:
        object_name = sport_params.point_source
    result_dict[suit_res.source_name].append(object_name)
    if simulation_dictionary[suit.source][source_ch.use_spectrum]:
        result_dict[suit_res.energy].append("spct")
    else:
        result_dict[suit_res.energy].append(simulation_dictionary[suit.source][source_ch.energy])
    result_dict[suit_res.optics_name].append(get_file_name(simulation_dictionary[suit.optics]["path"]))
    result_dict[suit_res.inclination].append(np.rad2deg(float(simulation_dictionary[suit.source][source_ch.inc]))*60)
    result_dict[suit_res.azimuth].append(np.rad2deg(float(simulation_dictionary[suit.source][source_ch.az]))*60)
    result_dict[suit_res.cross_area_simulated][suit_res.effective_area_total].append(round((effective_areas[0] + effective_areas[1]), 3))
    result_dict[suit_res.cross_area_simulated][suit_res.effective_area_focused].append(round(effective_areas[0], 3))
    result_dict[suit_res.cross_area_simulated][suit_res.effective_area_straylight].append(round(effective_areas[1], 3))
    #result_dict[suit_res.cross_area_analytical][suit_res.effective_area_total].append(round((effective_areas[2] + effective_areas[3]), 3))
    #result_dict[suit_res.cross_area_analytical][suit_res.effective_area_focused].append(round(effective_areas[2], 3))
    #result_dict[suit_res.cross_area_analytical][suit_res.effective_area_straylight].append(round(effective_areas[3], 3))
    result_dict[suit_res.half_energy_width][suit_res.hew_x].append(round(half_energy_width[0][0], 3))
    result_dict[suit_res.half_energy_width][suit_res.hew_x_units].append(half_energy_width[0][1])
    result_dict[suit_res.half_energy_width][suit_res.hew_y].append(round(half_energy_width[1][0], 3))
    result_dict[suit_res.half_energy_width][suit_res.hew_y_units].append(half_energy_width[1][1])
    return result_dict


def add_result(result_dict, file, index):
    with open(file, "a") as f:
        f.write(result_dict[suit_res.source_name][index] + "\t" + result_dict[suit_res.energy][index] + "\t")
        f.write("[eV]\t" + result_dict[suit_res.optics_name][index] + "\t")
        f.write(str(result_dict[suit_res.inclination][index]) + "\t" + str(result_dict[suit_res.azimuth][index]) + "\t")
        f.write(str(result_dict[suit_res.cross_area_simulated][suit_res.effective_area_focused][index]) + "\t")
        f.write(str(result_dict[suit_res.cross_area_simulated][suit_res.effective_area_straylight][index]) + "\t")
        f.write(str(result_dict[suit_res.cross_area_simulated][suit_res.effective_area_total][index]) + "\t")
        #f.write(str(result_dict[suit_res.cross_area_analytical][suit_res.effective_area_focused][index]) + "\t")
        #f.write(str(result_dict[suit_res.cross_area_analytical][suit_res.effective_area_straylight][index]) + "\t")
        #f.write(str(result_dict[suit_res.cross_area_analytical][suit_res.effective_area_total][index]) + "\t")
        f.write("[m^2]\t" + str(result_dict[suit_res.half_energy_width][suit_res.hew_x][index]) + "\t")
        f.write("[" + result_dict[suit_res.half_energy_width][suit_res.hew_x_units][index] + "]\t")
        f.write(str(result_dict[suit_res.half_energy_width][suit_res.hew_y][index]) + "\t")
        f.write("[" + result_dict[suit_res.half_energy_width][suit_res.hew_y_units][index] + "]\n")
    return


def export_results(result_dictionary, store_path):
    # Header
    with open(store_path + "/results.txt", "w+") as f:
        f.write("#SOURCE \tENERGY\t[UNITS]\tOPTICS\tINC\tAZ\tEA_FOC\tEA_STR\tEA_TOT\t")
        #f.write("EA_FOC_AN\tEA_STR_AN\tEA_TOT_AN\t")
        f.write("[UNITS]\tHEW_X\t[UNITS]\tHEW_Y\t[UNITS]\n")
        f.write("#-------------------------------------------------------------------------------------------------")
        f.write("---------------------------------------------\n")
    # Data
    for i in range(len(result_dictionary[suit_res.source_name])):
        add_result(result_dictionary, store_path + "/results.txt", i)
    return
