import os
import sys
import pickle as pk
import imaging as im
import numpy as np
import math
from namespaces import suit_analysis as suit
from namespaces import pickle_results as pk_res
from namespaces import csv_files
from namespaces import sport_params
from namespaces import csv_analysis_dict as csv_an
from namespaces import detector_characteristics as det_ch
from namespaces import module_characteristics as mod_ch
from namespaces import source_characteristics as src_ch
from namespaces import suit_paths
from sim_tools import read_csv, get_file_name


def load_pickle(pickle_path):
    """
    Load a pickle file given
    :param pickle_path: path to the pickle file
    :return: data stored in the pickle file
    """
    try:
        with open(pickle_path, 'rb') as pickle_file:
            sim_pickle = pk.Unpickler(pickle_file)
            sim_data = sim_pickle.load()
            pickle_file.close()
    except FileNotFoundError:
        sys.exit("Pickle file not found at %s." % pickle_path)
    return sim_data


def get_results(data):
    """
    Get simulation results from one pickle file
    :param data: data extracted from a pickle file
    :return: List of each simulation performed, divided in sources and monitors
    """
    simulation_results = []

    if len(data) == 2:
        # number of sources >= 1 and number of monitors > 1
        for result in data[pk_res.results]:
            aux_photon_list = split_photon_list(result[pk_res.sources], result[pk_res.particles])
            if len(result[pk_res.sources]) == 1:
                def_dict = {
                    suit.plane: result[pk_res.name],
                    suit.source: result[pk_res.sources][0],
                    suit.photon_list: aux_photon_list,
                    suit.optics: data[pk_res.optics]
                }
                simulation_results.append(def_dict)
            else:
                for i, source in enumerate(result[pk_res.sources]):
                    def_dict = {
                        suit.plane: result[pk_res.name],
                        suit.source: source,
                        suit.photon_list: aux_photon_list[i],
                        suit.optics: data[pk_res.optics]
                    }
                    simulation_results.append(def_dict)
    else:  # TODO: Finish this option
        # number of sources = 1 and number of monitors = 1
        def_dict = {
            suit.plane: "",
            suit.source: "",
            suit.photon_list: [],
            suit.optics: data[pk_res.optics]
        }
        simulation_results.append(def_dict)

    # TODO: I don't know if there is an option for sources > 1 monitor = 1 and if it will have a different structure

    return simulation_results


def process_images(simulation_results, store_path):
    """
    Generate output images for different monitors and using the information stored on 'analysis.csv'
    :param simulation_results: dictionary of results
    :param store_path: path to store the images
    :return:
    """
    for i, result in enumerate(simulation_results):
        monitor = result[suit.plane]
        print("Saving image of source %d monitor %s" % (i, monitor))
        if monitor == sport_params.focal_monitor:
            analysis_list = read_csv(csv_files.analysis)
            detector_frame = im.load_detector(analysis_list[0][csv_an.detector])
            im.image_processing(result, store_path, detector=detector_frame, zoom=True)
        else:
            im.image_processing(result, store_path)
    return


def get_fits_image(store_path):
    """
    Obtains the image stored in the fits file indicated on 'analysis.csv'
    :param store_path: path to store the image
    :return:
    """
    analysis_list = read_csv(csv_files.analysis)
    fits_file = get_file_name(analysis_list[0][csv_an.fits])
    im.original_image(fits_file, store_path)
    return


def create_image_folder(sim_name):
    try:
        folder_path = suit_paths.working_directory + "/results/" + sim_name
        os.mkdir(folder_path)
        print("Results folder: " + sim_name)
    except FileExistsError:
        folder_path = suit_paths.working_directory + "/results/" + sim_name
        print(folder_path + " already exists. Creating a new one.")
        i = 1
        folder_created = False
        while not folder_created:
            try:
                os.mkdir(folder_path + "_" + str(i))
                folder_created = True
                folder_path = folder_path + "_" + str(i)
                print("Results folder: " + sim_name + "_" + str(i))
            except FileExistsError:
                i += 1
    return folder_path


def split_photon_list(source_list, photon_list):
    split_list = []
    if len(source_list) == 1:
        return photon_list
    else:
        part_dict = {}
        for i in range(len(source_list)):
            part_dict["source_%d" % i] = create_particle_dictionary()
        for j, s_id in enumerate(photon_list[pk_res.source_id]):
            append_particle(part_dict["source_%s" % s_id], photon_list, j)
        for i in range(len(source_list)):
            split_list.append(part_dict["source_%d" % i])
    return split_list


def effective_area_sim(source, photon_list, optics):
    """
    Calculation of effective area computing the cross-sectional area running MC simulations (statistically biased)
    :param source: Information about the flux is taken from the source
    :param photon_list: List with all the photons to account for.
    :param optics: optics csv file from which the cross sectional area for a point source on axis is taken
    :return: Returns the value for the effective area for the given conditions in m^2
    """
    cross_sec_area = get_cs_area(optics)
    sum_photons = 0
    for w_i in photon_list[pk_res.particle_weight]:
        sum_photons += w_i
    eff_a = sum_photons / float(source[src_ch.flux])  # * cross_sec_area
    return eff_a


def effective_area_an(source, photon_list, optics):
    """
    Calculation of effective area computing the cross-sectional area analytically from a ing table (geom. approximation)
    :param source: Information about the flux is taken from the source
    :param photon_list: Information about the number of photons out is taken from the photon list
    :param optics: optics used in dictionary form with information of the ring table created by SPORT
    :return: Returns the value for the effective area for the given conditions in m^2
    """
    cross_sec_area = calculate_cs_area(optics)
    sum_photons = 0
    for w_i in photon_list[pk_res.particle_weight]:
        sum_photons += w_i
    eff_a = sum_photons / float(source[src_ch.flux])  # * cross_sec_area
    return eff_a


def get_cs_area(optics):
    list_optics = read_csv(csv_files.optics)
    file_name = get_file_name(optics["path"])
    optics_found = False
    for optic_i in list_optics:
        if file_name == optic_i["OPTICS"]:
            cs_area = float(optic_i["CS_AREA"])
            optics_found = True
            break
    if not optics_found:
        print("Cross sectional area of optics %s not available, returning A_cs = 0." % file_name)
        cs_area = 0
    return cs_area


def get_module_characteristics(optics):
    module_char = read_csv(csv_files.module_dimensions)
    module_op = get_file_name(optics["path"])
    module_found = False
    for module_i in module_char:
        if module_op == module_i[mod_ch.optics]:
            module = module_i
            module_found = True
            break
    if not module_found:
        print("Optics module not found for optics %s returning empty module." % module_op)
        module= {
            mod_ch.optics: "NOT FOUND",
            mod_ch.pWidth: 0,
            mod_ch.pHeight: 0,
            mod_ch.wall: 0,
            mod_ch.plate: 0,
            mod_ch.mHeight: 0
        }
    return module


def calculate_cs_area(sim_optics):
    ring_table = sim_optics["profile"]["ringTable"]
    sim_module = get_module_characteristics(sim_optics)
    pore_width = float(sim_module[mod_ch.pWidth])*1E-3
    pore_height = float(sim_module[mod_ch.pHeight])*1E-3
    wall_width = float(sim_module[mod_ch.wall])*1E-3
    plate_thickness = float(sim_module[mod_ch.plate])*1E-3
    module_height = float(sim_module[mod_ch.mHeight])*1E-3
    num_pores_y = module_height/(pore_height+plate_thickness) - 2

    cs_area = 0
    for ring_i in ring_table:
        num_pores_x = (float(ring_i[1])*1E-3 - wall_width)/(pore_width + wall_width)
        total_pores = int(num_pores_x) * int(num_pores_y)
        extra_pore_width = (pore_width + wall_width)*(num_pores_x - int(num_pores_x)) - wall_width
        module_area = (total_pores*pore_height*pore_width) + (num_pores_y*pore_height*extra_pore_width)
        ring_area = module_area*float(ring_i[4])  # Pos 4 in SPORT ring table indicates number of modules per ring
        cs_area += ring_area
    return cs_area


def compute_eff_area(sim_results, option="fs"):
    """
    Computes the effective area for different options
    :param sim_results: results of a certain simulation
    :param option: setting the light and cross sectional area conditions
        - "fs" (default): focused and simulated cross sectional area
        - "fc": focused and analytically computed cross sectional area
        - "ss": straylight and simulated cross sectional area
        - "sc": straylight and analytically computed cross sectional area
    :return: effective area value
    """
    photon_list = sim_results[suit.photon_list]
    photon_list_effective = create_particle_dictionary()
    if option == "fs":
        for i in range(len(photon_list[pk_res.particle_position])):
            if im.is_focused(photon_list, i):
                photon_list_effective = append_particle(photon_list_effective, photon_list, i)
        eff_area = effective_area_sim(sim_results[suit.source], photon_list_effective, sim_results[suit.optics])
    elif option == "fc":
        for i in range(len(photon_list[pk_res.particle_position])):
            if im.is_focused(photon_list, i):
                photon_list_effective = append_particle(photon_list_effective, photon_list, i)
        eff_area = effective_area_an(sim_results[suit.source], photon_list_effective, sim_results[suit.optics])
    elif option == "ss":
        for i in range(len(photon_list[pk_res.particle_position])):
            if im.is_straylight(photon_list, i):
                photon_list_effective = append_particle(photon_list_effective, photon_list, i)
        eff_area = effective_area_sim(sim_results[suit.source], photon_list_effective, sim_results[suit.optics])
    elif option == "sc":
        for i in range(len(photon_list[pk_res.particle_position])):
            if im.is_straylight(photon_list, i):
                photon_list_effective = append_particle(photon_list_effective, photon_list, i)
        eff_area = effective_area_an(sim_results[suit.source], photon_list_effective, sim_results[suit.optics])
    else:
        eff_area = 0
        print("Simulation mode %s not supported." % option)
    return eff_area


def compute_hew(sim_results):
    analysis_list = read_csv(csv_files.analysis)
    detector = im.load_detector(analysis_list[0][csv_an.detector])
    focal_length = float(sim_results[suit.optics]["geometry"]["fl"])

    list_x = get_axiscoord(sim_results, 0)
    list_y = get_axiscoord(sim_results, 1)
    list_w = get_weights(sim_results)

    intensity_hist = np.histogram2d(list_x, list_y, bins=detector[det_ch.pixels], range=detector[det_ch.size],
                                    weights=list_w)[0]
    hew_axes = [hew_axis(intensity_hist, axis=0), hew_axis(intensity_hist, axis=1)]
    hew_tot = []
    for i, hew_ax in enumerate(hew_axes):
        det_cent = detector[det_ch.pixels][i] / 2
        beta_2 = math.atan((hew_ax[2] - det_cent) * detector[det_ch.pixel_size] / focal_length)
        beta_1 = math.atan((hew_ax[0] - det_cent) * detector[det_ch.pixel_size] / focal_length)
        hew = np.rad2deg(beta_2 - beta_1) * 60
        hew_unit = "arcmin"
        if hew < 1:
            hew = hew*60
            hew_unit = "arcsec"
        hew_tot.append([hew, hew_unit])
    return hew_tot


def hew_axis(sim_hist, axis=0):
    list_max = []
    for i in range(sim_hist.shape[axis]):
        if axis == 0:
            list_max.append(max(sim_hist[i, :]))
        elif axis == 1:
            list_max.append(max(sim_hist[:, i]))
    smoothed_profile = av_smooth(list_max, n=1)
    max_index = np.argmax(smoothed_profile)
    hew_pix = [get_hew_index(smoothed_profile, max_index, direction="left"), max_index,
               get_hew_index(smoothed_profile, max_index, direction="right")]
    return hew_pix


def get_hew_index(data, index, direction="left"):

    pos_flag = False
    i = index
    while not pos_flag:
        val_i = data[i]
        if val_i <= data[index]/2:
            pos_flag = True
            hew_index = i
        else:
            if direction == "left":
                i -= 1
            elif direction == "right":
                i += 1
            else:
                sys.exit("Please introduce a proper direction (left or right).")
    return hew_index


def av_smooth(data, n):

    if n == 0:
        return data

    smoothed_fx = []
    for i in range(len(data)):
        values = data[max(0, i-n):min(i+n, len(data) - 1)]
        smoothed_fx.append(sum(values)/len(values))

    return smoothed_fx


def get_axiscoord(sim_results, axis):
    photon_list = sim_results[suit.photon_list]
    axis_list = []
    for photon in photon_list[pk_res.particle_position]:
        axis_list.append(photon[axis])
    return axis_list


def get_weights(sim_results):
    photon_list = sim_results[suit.photon_list]
    weight_list = []
    for w_i in photon_list[pk_res.particle_weight]:
        weight_list.append(w_i)
    return weight_list


def create_particle_dictionary():
    particle_dictionary = {
        pk_res.particle_position: [],
        pk_res.wavevector: [],
        pk_res.particle_weight: [],
        pk_res.energy: [],
        pk_res.particle_trajectory: [],
        pk_res.mod: [],
        pk_res.source_id: [],
        pk_res.incidence_vector: []
    }
    return particle_dictionary


def append_particle(par_dic, photon_list, index):
    par_dic[pk_res.particle_position].append(photon_list[pk_res.particle_position][index])
    par_dic[pk_res.wavevector].append(photon_list[pk_res.wavevector][index])
    par_dic[pk_res.particle_weight].append(photon_list[pk_res.particle_weight][index])
    par_dic[pk_res.energy].append(photon_list[pk_res.energy][index])
    par_dic[pk_res.particle_trajectory].append(photon_list[pk_res.particle_trajectory][index])
    par_dic[pk_res.mod].append(photon_list[pk_res.mod][index])
    par_dic[pk_res.source_id].append(photon_list[pk_res.source_id][index])
    par_dic[pk_res.incidence_vector].append(photon_list[pk_res.incidence_vector][index])
    return par_dic
