from math import sin, asin, cos, acos, tan, atan, atan2, pi, sqrt
from astropy.io import fits
import numpy as np
from numpy.linalg import inv
import debug
from names import params, fits_header


# Read a FITS file
def readFile(path, frame=None):
    file = fits.open(path)

    # If the frame is specified, return that frame
    if frame != None:
        file = getFitsFrame(file, frame)

    #If frame is not specified, but there is only 1 frame, return that frame
    elif len(file) == 1:
        file = getFitsFrame(file, 0)

    return file


def getFitsFrameCount(file):
    return len(file)


# Get a frame from a FITS file, given an index
def getFitsFrame(file, frame):
    # Return the frame given by the index if it exists, otherwise return the
    # first frame
    try:
        out = file[frame]
    except IndexError:
        debug.warning("Can't access frame # %d. Returning first frame." % frame)
        out = file[0]
    return out


# Return the data table of a FITS frame
def getFrameData(frame):
    return frame.data


# Retrieve key parameters from the header of a FITS frame, given a telescope
# profile
def getDimensions(frame):
    # Read parameters from the header
    wPixels = frame.header[fits_header.jAxPixels % 1]
    hPixels = frame.header[fits_header.jAxPixels % 2]

    refx = frame.header[fits_header.jAxRefPixel % 1]
    refy = frame.header[fits_header.jAxRefPixel % 2]

    ra = frame.header[fits_header.iAxRefVal % 1]/180*pi
    dec = frame.header[fits_header.iAxRefVal % 2]/180*pi

    # Since there are two possible formalisms to transform, both are checked
    if fits_header.linTransPC % (1, 1) in frame.header:
        rotation = fits_header.linTransPC
        ax = frame.header[fits_header.iAxRscToPhys % 1]
        ay = frame.header[fits_header.iAxRscToPhys % 2]
    elif fits_header.linTransCD % (1, 1) in frame.header:
        rotation = fits_header.linTransCD
        # Since the C_Di_j formalism combines the rescaling, its values are set to 1 so doesn't interfere in the code
        ax = 1
        ay = 1
    else:
        rotation = None

    # if no rotation was found, make the rotation matrix an identity matrix
    if rotation == None:
        print("No formalism specified in fits file, assumed P_Ci_j and setting to identity matrix")
        rm = np.identity(2)
        ax = frame.header[fits_header.iAxRscToPhys % 1]
        ay = frame.header[fits_header.iAxRscToPhys % 2]
    # if a rotation was found, read it into a rotation matrix
    else:
        rm = np.zeros((2,2))
        for i in range(0, 2):
            for j in range(0, 2):
                rm[i][j] = frame.header[rotation % (i+1, j+1)]

    # Store all parameters into a dictionary and return it
    out = {}
    out[params.widthPixels] = wPixels
    out[params.heightPixels] = hPixels
    out[params.xRefPixel] = refx
    out[params.yRefPixel] = refy
    out[params.rectascension] = ra
    out[params.declination] = dec
    out[params.pixelWidthDegrees] = ax
    out[params.pixelHeightDegrees] = ay
    out[params.rotationMatrix] = rm

    return out

# Find the greatest angular distance from the reference point of a FITS frame to
# any pixel in the frame
def getMaxDegreesFromOrigin(frame):
    d = getDimensions(frame)

    # The maximum x and y values of the frame, scaled to degrees
    xMax = d[params.widthPixels]*d[params.pixelWidthDegrees]
    yMax = d[params.heightPixels]*d[params.pixelHeightDegrees]

    # The reference point scaled to degrees
    xRef = d[params.xRefPixel]*d[params.pixelWidthDegrees]
    yRef = d[params.yRefPixel]*d[params.pixelHeightDegrees]

    # Assume that the maximum distance will be to one of the four corners of the
    # image, so check these points and find the greatest distance
    dMax = max(\
        dist(0, 0, xRef, yRef),\
        dist(xMax, yMax, xRef, yRef),\
        dist(0, 0, xRef, yRef),\
        dist(xMax, yMax, xRef, yRef)\
    )
    return dMax

# Calculate the distance between two points
def dist(x1, y1, x2, y2):
    return sqrt((x2-x1)**2 + (y2-y1)**2)

# FITS transformation functions ================================================
# Get the CD matrix of a FITS file, which encodes both rotation of the frame and
# the scaling from pixels to degrees
def getCD(frame):
    dims = getDimensions(frame)
    sx = dims[params.pixelWidthDegrees]
    sy = dims[params.pixelHeightDegrees]
    # get the rotation matrix of the image, then scale it
    return np.array([[sx, 0],[0, sy]]).dot(dims[params.rotationMatrix])

# Get the inverted CD matrix to convert from world coordinates to image coordinates
def getCDInverted(frame):
    return inv(getCD(frame))

# Get the rotation matrix that the image is rotated by - this corresponds to the
# telescope's rotation around the optical axis
def getImageRotation(frame):
    dims = getDimensions(frame)
    r = dims[params.rotationMatrix]
    return atan2(r[1][0], r[0][0])

# Given a viewing direction in spherical coordinates, get the basis of the
# cartesian coordinate system with the z-axis along the provided vector, and the
# y-axis upward (i.e. with the highest possible z-coordinate)
def getViewBasis(viewDirection):
    # Rotating the z-axis in place
    base = Base(0, float(-viewDirection[1])+pi/2, float(viewDirection[2]))
    # Rotation of 90 degrees around z axis to align the x-y axes properly
    x = base.getColumn(0)
    y = base.getColumn(1)
    base.setColumn(0, y)
    base.setColumn(1, -x)
    return base

# Get the basis of the cartesian coordinate system whose z-axis points toward
# the projection center of the frame
# TODO: THIS MAY STILL BE WRONG?
def getFrameBasis(frame):
    dims = getDimensions(frame)
    frameOrigin = [1, dims[params.declination], dims[params.rectascension]]
    # Cartesian coordinate system aligned with the telescope's viewing direction
    return getViewBasis(frameOrigin)

def getSourceFrameBasis(source):
    sourceOrigin = [1, source.getInclination(), source.getAzimuth()]
    return getViewBasis(sourceOrigin)

# Calculate the transform from the frame basis to the telescope's basis (z-axis
# aligned with optical axis)
def getFrameToViewTransform(frame, viewDirection, viewRotation):
    frameBasis = getFrameBasis(frame)
    viewBasis = getViewBasis(viewDirection)
    return rotateZ(-viewRotation).dot(getTransform(frameBasis, viewBasis))

# Calculate the transform from the telescope's basis to the frame basis
def getViewToFrameTransform(frame, viewDirection, viewRotation):
    # get the reverse transform and invert it
    return inv(getFrameToViewTransform(frame, viewDirection, viewRotation))

def getSourceToViewTransform(source, viewDirection, viewRotation):
    sourceBasis = getSourceFrameBasis(source)
    viewBasis = getViewBasis(viewDirection)
    return rotateZ(-viewRotation).dot(getTransform(sourceBasis, viewBasis))

def getViewToSourceTransform(source, viewDirection, viewRotation):
    return inv(getSourceToViewTransform(source, viewDirection, viewRotation))

# Calculate the position of the projection origin of the frame, in spherical
# coordinates in the telescope basis
def getFrameOriginInViewVector(frame, viewDirection, viewRotation):
    # calculate the transform from the frame to the view basis
    T = getFrameToViewTransform(frame, viewDirection, viewRotation)

    # convert the transform's z-column to spherical coordinates
    return cartesianToPolar(T[:,2])

def getManualOriginInViewVector(source, viewDirection, viewRotation):
    T = getSourceToViewTransform(source, viewDirection, viewRotation)
    return cartesianToPolar(T[:, 2])

# Convert pixels in a FITS frame to equatorial coordinates on the sky
def imageToSky(frame, px, py):
    # Calculate the pixel coordinates relative to the projection origin
    dims = getDimensions(frame)
    pRelPixel = np.array([px-dims[params.xRefPixel], py-dims[params.yRefPixel]])

    # Scale and rotate the pixels according to the CD matrix of the frame
    CD = getCD(frame)
    pRel = CD.dot(pRelPixel) #Relative position of pixel in angles
    pRelRad = np.array(pRel)*pi/180

    # Calculate the polar coordinates of the relative point
    phi = atan2(pRelRad[1], pRelRad[0])
    theta = atan(sqrt(pRelRad[0]**2 + pRelRad[1]**2))
    pPolar = [1, pi/2-theta, phi] # Not actually polar, but spherical with the pole at the reference point

    # Cartesian coordinates of the pixel as projected onto the celestial dome,
    # if the optical axis aligned with the origin of the spherical coordinate system
    pRelCart = polarToCartesian(pPolar)

    telescopeBasis = getFrameBasis(frame)

    # Cartesian coordinates of the pixels as projected onto the celestial dome,
    # with the optical axis rotated to its proper equatorial coordinates
    pAbsCart = telescopeBasis.dot(pRelCart)

    # Spherical coordinates of the pixel in the equatorial coordinate system
    pSky = cartesianToPolar(pAbsCart)
    showSphericalVector(pSky, "imageToSky")
    return pSky

# Convert equatorial coordinates on the sky to pixels in a FITS frame
def skyToImage(frame, ra, dec):
    # Convert the equatorial coordinates to cartesian coordinates, assuming a radius of 1
    pAbsCart = polarToCartesian([1, dec, ra])

    # Transform the cartesian coordinates to the basis of the telescope
    telescopeBasis = getFrameBasis(frame)
    pRelCart = transformTo(pAbsCart, telescopeBasis)

    # Reproject the point, relative to the telescope, onto the image plane
    pPolar = cartesianToPolar(pRelCart)
    pPolar[1] = -pPolar[1] + pi/2
    x = tan(pPolar[1])*cos(pPolar[2])
    y = tan(pPolar[1])*sin(pPolar[2])
    pRelRad = np.array([x, y])
    pRel = pRelRad/pi*180

    # Rotate and scale the point to the image's original scale and rotation
    CD = getCD(frame)
    pRelPixel = inv(CD).dot(pRel)
    dims = getDimensions(frame)

    # Add the pixel coordinates of the projection origin to make the point's
    # pixel coordinates between 0 and the width or height of the frame
    px = pRelPixel[0] + dims[params.xRefPixel]
    py = pRelPixel[1] + dims[params.yRefPixel]
    return [px, py]


# Transforms points between spherical coordinate systems, given the cartesian
# bases they inhabit
# All bases are relative to that of the equatorial coordinate system. If no old
# basis is given, the point is assumed to originate from the equatorial system
def transformSpherical(point, newBase, oldBase=None):
    # Convert the spherical point to cartesian coordinates
    pointC = cartesianToPolar(point)

    # Transform the point to rebase it to the new coordinate system
    if oldBase==None:
        pointR = transformTo(point, newBase)
    else:
        pointR = transform(point, oldBase, newBase)

    # Convert the point back to spherical coordinates
    return polarToCartesian(pointR)

# Convert a point from spherical coordinates to cartesian coordinates
def polarToCartesian(p):
    x = p[0]*cos(p[1])*cos(p[2])
    y = p[0]*cos(p[1])*sin(p[2])
    z = p[0]*sin(p[1])
    return [x, y, z]

# Convert a point from cartesian coordinates to spherical coordinates
def cartesianToPolar(p):
    r = sqrt(p[0]**2 + p[1]**2 + p[2]**2)
    xy = sqrt(p[0]**2 + p[1]**2)
    theta = atan2(p[2], xy)
    phi = atan2(p[1], p[0])
    return [r, theta, phi]

# Create a 3D scaling transform matrix
def scale(s):
    return np.array([[s, 0, 0], [0, s, 0], [0, 0, s]])

# Create a 3D rotation matrix around the X axis
def rotateX(a):
    return np.array([[1, 0, 0], [0, cos(a), sin(a)], [0, -sin(a), cos(a)]])

# Create a 3D rotation matrix around the Y axis
def rotateY(a):
    return np.array([[cos(a), 0, sin(a)], [0, 1, 0], [-sin(a), 0, cos(a)]])

# Create a 3D rotation matrix around the Z axis
def rotateZ(a):
    return np.array([[cos(a), -sin(a), 0], [sin(a), cos(a), 0], [0, 0, 1]])

# Utility function, print the declination and rectascension of a spherical vector
def showSphericalVector(vin, name="sv", **kwargs):
    v = [vv for vv in vin]
    if kwargs.get("radians", True):
        v[1] = v[1] * 180/pi
        v[2] = v[2] * 180/pi

    print(name, ":", v)

# Utility function, print the axis and angle representation of a 3D rotation
# matrix
def showAxisAngle(T):
    axis = np.array([\
        T[2][1] - T[1][2],\
        T[0][2] - T[2][0],\
        T[1][0] - T[0][1]\
    ])
    angle = acos((sum([T[i][i] for i in range(0, 3)])-1)/2)

    print("Axis: ", axis/sqrt(sum([x**2 for x in axis])))
    print("Angle: ", angle*180/pi)

# Base is a wrapper class for a 3x3 numpy array which represents the basis for
# a cartesian coordinate system. It has a few utility functions, and getting and
# setting functions for individual columns.
# Base is initialized with 3 angles in radians, which represent around the X, Y
# and Z axes, in that order
class Base():
    def __init__(self, ax, ay, az):
        # Construct a unit basis and rotate it according to the provided angles
        self.m = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        self.m = rotateZ(az).dot(rotateY(ay)).dot(rotateX(ax)).dot(self.m)

    # Premultiply this basis as a matrix onto a given vector or matrix and return
    # the result
    def dot(self, a):
        return self.m.dot(a)

    # Return the inverse of the basis matrix
    def inv(self):
        return inv(self.m)

    # Return a column of the basis matrix by index
    def getColumn(self, index):
        return np.array([r[index] for r in self.m])

    # Set a column of the matrix by index
    def setColumn(self, index, col):
        for i in range(0, 3):
            self.m[i][index] = col[i]


# Helper functions to calculate transformation matrices and transform points
# to rebase them
def transformFrom(point, base):
    return base.dot(point)


def transformTo(point, base):
    print(point)
    return base.inv().dot(point)


def transform(point, from_, to_):
    return getTransform(from_, to_).dot(point)


# Calculate the transform matrix between coordinate systems
def getTransform(from_, to_):
    return inv(to_.m).dot(from_.m)
