#!/bin/tcsh
set PYTHONVERSION = "3.8.0"
set PYTHONFOLDERNAME = "Python-"${PYTHONVERSION}
set PYTHONBIN = ${PYHTONFOLDERNAME}"/python"

#tar -xvzf ./Python-3.8.0.tar.xz
#cd ./Python-3.8.0
#chmod +x ./configure
#./configure
#make
#make test
#cd ..

#./${PYTHONBIN} ./get-pip.py --user
setenv PATH ${PATH}:"~/.local/bin:~/.local/lib"

set PIP = "~/.local/bin/pip3.8"
./${PYTHONBIN} ${PIP} install -t /home/dipasa/SUIT/py/deps --ignored-installed Pillow astropy matplotlib
