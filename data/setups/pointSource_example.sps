{
    "sources": [
        {
            "type": "pointSource",
            "id": "0",
            "inclination": "0.00002424068405547679873455218726",
            "azimuth": "0.00002424112523592660841228915611",
            "energy": "6000",
            "useSpectrum": false,
            "flux": "1"
        }
    ],
    "monitors": [
        {
            "monitorType": "focalPlane",
            "round": false,
            "x": "0",
            "y": "0",
            "z": "0",
            "width": "1",
            "height": "1",
            "name": "focalPlane"
        }
    ]
}