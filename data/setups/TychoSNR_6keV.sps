{
    "sources": [
        {
            "type": "extendedSource",
            "id": "0",
            "inclination": "0",
            "azimuth": "0",
            "energy": "6000",
            "useSpectrum": false,
            "flux": 79.91422271728516,
            "filepath": "/home/arnsaa/shared/Users/dipasa/SPORT/save/save/extendedSources/m1cntrat.fits",
            "filename": "m1cntrat.fits",
            "frame": 1
        }
    ],
    "monitors": [
        {
            "monitorType": "focalPlane",
            "round": false,
            "x": "0",
            "y": "0",
            "z": "0",
            "width": "1",
            "height": "1",
            "name": "focalPlane"
        }
    ]
}