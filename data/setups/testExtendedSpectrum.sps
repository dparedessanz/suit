{
    "sources": [
        {
            "type": "extendedSource",
            "id": "1",
            "inclination": "0",
            "azimuth": "0",
            "energy": "1",
            "useSpectrum": true,
            "flux": 79.91422271728516,
            "spectrum": {
                "sourceTablePath": "save/spectra/uko9v.csv",
                "xColumn": 0,
                "xUnit": "ev",
                "intensityColumn": 1,
                "intensityUnit": "photonflux"
            },
            "filepath": "/home/arne/sport/save/extendedSources/m1cntrat.fits",
            "filename": "m1cntrat.fits",
            "frame": 1,
            "averageNonZeroFlux": 0.00243618640725803
        }
    ],
    "monitors": [
        {
            "monitorType": "focalPlane",
            "round": false,
            "x": "0",
            "y": "0",
            "z": "0",
            "width": "1",
            "height": "1",
            "name": "focalPlane"
        }
    ]
}
