

def linear_interpolation(x_list, y_list, x_int):
    x_i_min = 0
    x_i_max = len(x_list) - 1

    if x_int < x_list[x_i_min]:
        y_int = linear_extrapolation(x_list, y_list, x_int, "left")
        return y_int
    elif x_int > x_list[x_i_max]:
        y_int = linear_extrapolation(x_list, y_list, x_int, "right")
        return y_int
    else:
        i_found = False
        while not i_found:
            mid_i = int((x_i_max + x_i_min)/2)
            if x_int <= x_list[mid_i]:
                x_i_max = mid_i
            else:  # x_int > x_list[mid_i]:
                x_i_min = mid_i

            if x_i_min == x_i_max - 1:
                i_found = True
    y_int = (x_int - x_list[x_i_min]) * (y_list[x_i_max] - y_list[x_i_min]) / (x_list[x_i_max] - x_list[x_i_min]) + y_list[x_i_min]
    return y_int


def linear_extrapolation(x_list, y_list, x_ext, loc):
    if loc == "left":
        m = (y_list[1] - y_list[0])/(x_list[1] - x_list[0])
        dx = (x_ext - x_list[0])
        y_ext = dx * m + y_list[0]

    else:  # loc == "right"
        m = (y_list[len(x_list) - 1] - y_list[len(x_list) - 2]) / (x_list[len(x_list) - 1] - x_list[len(x_list) - 2])
        dx = (x_ext - x_list[len(x_list) - 1])
        y_ext = dx * m + y_list[len(x_list) - 1]
    return y_ext
