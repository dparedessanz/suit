import subprocess
import sys
import analysis
import result
from sim_tools import sim_file_creation, run_simulation, read_csv, get_num_optics, split_simulation_list
from sim_tools import move_file, get_file_name, remove_extension, wipe_temp
from namespaces import csv_files
from namespaces import csv_simulation_dict as sim
from namespaces import csv_analysis_dict as csv_analysis
from namespaces import pickle_results as pk_res
from namespaces import suit_analysis as suit
from namespaces import csv_analysis_dict as csv_an
from namespaces import sport_params
from namespaces import suit_paths



def main():
    arguments = {}
    run_sim_flag = False
    i = 1

    while i < len(sys.argv):
        a = sys.argv[i]
    
        if a == "-h":
            help_pipeline()
            return

        elif a == "-r":
            run_sim_flag = True

        else:
            print("Unrecognized argument %s" % a)

        i += 1

    platform_system = sys.platform
    if platform_system == "linux" or platform_system == "linux2":
        pypath = suit_paths.working_directory + "/py/Python-3.8.0/python"
    else:  # We must be in windows
        sys.exit("Platform not supported yet")

    # ------------------------------------------------ RUN SIMULATION ------------------------------------------------
    # Read simulation.csv and analysis.csv files to create '.sps' file, 'sim_run.sh'
    analysis_dict = read_csv(csv_files.analysis)
    if run_sim_flag:
        # Clean temp folder before running simulation to avoid errors
        wipe_temp()
        simulation_order = read_csv("simulations/simulation.csv")
        if get_num_optics(simulation_order) > 1:
            # TODO: Implement running different optics
            sim_code = 1
            simulation_csv = split_simulation_list(simulation_order)

        else:
            # Use the first simulation to set optics file and create setup file
            sim_code = 2
            optics_path = simulation_order[0][sim.optics]  # Location of the file in Thinlinc
            subprocess.call(pypath + " create_sps.py -csv", shell=True)
            with open("simulations/temp/setup_data.txt", "r") as f:
                sim_setup_file = f.read()
            sim_name = remove_extension(get_file_name(sim_setup_file))
            # If the simulations have different rays then do a loop and generate different pickle files.
            sim_file_creation(platform_system, optics_path, sim_setup_file, num_rays=simulation_order[0][sim.rays],
                              out_name=sim_name)
            run_simulation(platform_system)

            pickle_file = sim_name + ".pickle"
            result_file = sim_name + ".spr"
            mcpl_folder = sim_name
            res_path = "./data/outputs/"
            dst_path_pk = "./data/outputs/pickles/"
            dst_path_spr = "./data/outputs/sportout/"
            pickle_path = dst_path_pk + pickle_file
            move_file(pickle_file, res_path, dst_path_pk)
            move_file(result_file, res_path, dst_path_spr)
            move_file(mcpl_folder, res_path, dst_path_spr)
            wipe_temp(sim_code)
            print("Storing of data finished.")
    else:
        print("Analysis mode only.")
        pickle_path = analysis_dict[0][csv_an.pickle]
        optics_path = analysis_dict[0][csv_an.optics]
        sim_name = "analysis_" + remove_extension(get_file_name(pickle_path))

    print("File: " + get_file_name(pickle_path))
    print("Optics: " + get_file_name(optics_path))
    print("Detector: " + get_file_name(analysis_dict[0][csv_an.detector]) + "\n")

    # ----------------------------------------------- READ PICKLE FILE ------------------------------------------------
    print("Analysis starting...")
    # TODO: More than one optic will generate more than one pickle file
    # for pickle_i in list_of_pickles
    sim_data = analysis.load_pickle(pickle_path)
    sim_data[pk_res.optics]["path"] = optics_path
    sim_results = analysis.get_results(sim_data)

    # ------------------------------------------------ POST-PROCESSING ------------------------------------------------
    # Effective area and HEW calculations
    simulations_ea = []
    simulations_hew = []
    av_options = ["fs", "ss"]
    for sim_i in sim_results:
        eff_areas = []
        if sim_i[suit.plane] == sport_params.focal_monitor:
            for op in av_options:
                eff_a = analysis.compute_eff_area(sim_i, op)
                eff_areas.append(eff_a)
            simulations_ea.append(eff_areas)
            simulations_hew.append(analysis.compute_hew(sim_i))

    # Image processing
    store_results_path = analysis.create_image_folder(sim_name)

    if analysis_dict[0][csv_analysis.fits] != "None":
        analysis.get_fits_image(store_results_path)
    analysis.process_images(sim_results, store_results_path)
    print("Analysis finished.")

    # ------------------------------------------------ EXPORT RESULTS -------------------------------------------------
    print("Saving results...")
    result_dict = result.create_result_dictionary()
    j = 0
    for i, sim_i in enumerate(sim_results):
        if sim_i[suit.plane] == sport_params.focal_monitor:
            result.append_result(result_dict, sim_i, simulations_ea[j], simulations_hew[j])
            j += 1
    result.export_results(result_dict, store_results_path)
    print("Done.")
    return


def help_pipeline():
    print("SUIT\n\n")
    print("Available options:")
    print("-r: run simulation, a name for the results must be included as a following argument")
    return


if __name__ == "__main__":
    main()
