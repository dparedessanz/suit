import os.path
import sys
import json
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from matplotlib.colors import LogNorm, PowerNorm
from copy import copy
from astropy.io import fits as astrofits
from create_sps import sourcesFromArgs
from namespaces import detector_characteristics as det_ch
from namespaces import pickle_results as pk_res
from namespaces import suit_analysis as suit
from namespaces import sport_params


def original_image(fits_file, store_path):
    platform_system = sys.platform
    fits_ns = sourcesFromArgs(platform_system, fits_file)

    with astrofits.open(fits_ns.workingPath) as file_read:
        fits_data = fits2py(file_read[fits_ns.frame].data)

    image_matrix = one_colour_filter(fits_data)
    original_img = Image.fromarray(image_matrix)
    original_img.save(store_path + "/" + fits_ns.object + "_original.png")
    return


def image_processing(result_dict, store_path, detector=None, zoom=False, zf=1):
    sim_pos_x = []
    sim_pos_y = []
    sim_weight = []
    i = 0
    photon_list = result_dict[suit.photon_list]
    monitor = result_dict[suit.plane]
    for par in photon_list[pk_res.particle_position]:
        if is_focused(photon_list, i):
            sim_pos_x.append(par[0])
            sim_pos_y.append(par[1])
            sim_weight.append(photon_list[pk_res.particle_weight][i])
        i += 1

    cmap = copy(plt.get_cmap('bone'))
    cmap.set_bad(cmap(0))
    save_path = store_path + "/" + "source%s" % photon_list[pk_res.source_id][0] + "_" + monitor

    sim_pos_x, sim_pos_y = rotation_filter(sim_pos_x, sim_pos_y, rot=180)
    if monitor == sport_params.focal_monitor and detector is not None:
        fig = plt.figure('Processed Image')
        plt.hist2d(sim_pos_x, sim_pos_y, bins=detector[det_ch.pixels], range=detector[det_ch.size], density=False,
                   weights=sim_weight, norm=PowerNorm(gamma=0.7), cmap=cmap)
        plt.xlabel('[m]')
        plt.ylabel('[m]')
        plt.colorbar()
        plt.savefig(save_path + '.png', bbox_inches='tight')
        plt.close(fig)
        del fig

        if zoom:
            im_edges = [[np.min(sim_pos_x), np.max(sim_pos_x)], [np.min(sim_pos_y), np.max(sim_pos_y)]]
            im_ref = [round((im_edges[0][1] + im_edges[0][0]) / 2, 5), round((im_edges[1][1] + im_edges[1][0]) / 2, 5)]
            sq_max = round(np.max(im_edges), 5)
            zoom_range = [[(im_ref[0] - zf * sq_max), (im_ref[0] + zf * sq_max)],
                          [(im_ref[1] - zf * sq_max), (im_ref[1] + zf * sq_max)]]
            x_pix_zoom = int((zoom_range[0][1] - zoom_range[0][0]) / detector[det_ch.pixel_size])
            y_pix_zoom = int((zoom_range[1][1] - zoom_range[1][0]) / detector[det_ch.pixel_size])
            zoom_pix = [x_pix_zoom, y_pix_zoom]

            fig = plt.figure('Zoomed image')
            plt.hist2d(sim_pos_x, sim_pos_y, bins=zoom_pix, range=zoom_range, density=False,
                       weights=sim_weight, norm=PowerNorm(gamma=0.7), cmap=cmap)
            plt.xlabel('[m]')
            plt.ylabel('[m]')
            plt.colorbar()
            plt.savefig(save_path + '_zoom.png', bbox_inches='tight')
            plt.close(fig)
            del fig
    elif monitor == sport_params.focal_monitor and detector is None:
        sys.exit("A detector must be chosen for monitor %s" % monitor)
    else:
        fig = plt.figure('Processed Image')
        plt.hist2d(sim_pos_x, sim_pos_y, bins=[501, 501], density=False,
                   weights=sim_weight, norm=PowerNorm(gamma=0.7), cmap=cmap)
        plt.xlabel('[m]')
        plt.ylabel('[m]')
        plt.colorbar()
        plt.savefig(save_path + '.png', bbox_inches='tight')
        plt.close(fig)

    return


def load_detector(det_path):
    """
    This function reads the data from a json-like file that contains information about the detector and returns a
    detector dictionary.
    """
    if os.path.isfile(det_path):
        with open(det_path, "r") as read_file:
            detector_data = json.load(read_file)
    else:
        sys.exit("Detector data not available in %s." % det_path)
    return detector_data


def create_detector(pixel_size, pixel_number, name="detector", detType="NA", telescope="NA", plate_loc=[1, 1], pix_gap=0,
                    plate_gap=0):
    # Creates json detector file.
    # The detector is centered in x=0, y=0
    pixels = [int(pixel_number[0] * plate_loc[1]), int(pixel_number[1] * plate_loc[0])]
    plate_size = [(pixel_number[0] * pixel_size) + (pixel_number[0] - 1) * pix_gap,
                  (pixel_number[1] * pixel_size) + (pixel_number[1] - 1) * pix_gap]
    size_x = plate_loc[1] * plate_size[0] + (plate_loc[1] - 1) * plate_gap
    size_y = plate_loc[0] * plate_size[1] + (plate_loc[0] - 1) * plate_gap
    size = [[-1 * size_x / 2, size_x / 2], [-1 * size_y / 2, size_y / 2]]
    json_data = {
        "info": {
            det_ch.name: name,
            det_ch.type: detType,
            det_ch.telescope: telescope
        },
        det_ch.pixels: pixels,
        det_ch.size: size,
        det_ch.pixel_size: pixel_size,
        det_ch.pixel_gap: pix_gap,
        det_ch.plate_location: plate_loc,
        det_ch.plate_gap: plate_gap
    }
    file_path = "./data/detector/" + name
    if not os.path.isfile(file_path + ".spd"):
        with open(file_path + ".spd", "w") as write_json:
            json.dump(json_data, write_json, indent=4)
    else:
        i = 1
        while os.path.isfile(file_path + "_" + str(i) + ".spd"):
            i += 1

        with open(file_path + "_" + str(i) + ".spd", "w") as write_json:
            json.dump(json_data, write_json, indent=4)
    return


def is_focused(photon, num):
    if photon[pk_res.particle_trajectory][num] == 73:
        return True
    else:
        return False


def is_straylight(photon, num):
    if is_focused(photon, num):
        return False
    else:
        return True


def rotation_filter(x, y, rot=0):
    x_rot = []
    y_rot = []
    for i in range(np.size(x)):
        x_rot_val = x[i] * np.cos(np.deg2rad(rot)) - y[i] * np.sin(np.deg2rad(rot))
        y_rot_val = y[i] * np.cos(np.deg2rad(rot)) + x[i] * np.sin(np.deg2rad(rot))
        x_rot.append(x_rot_val)
        y_rot.append(y_rot_val)
    return x_rot, y_rot


def one_colour_filter(image_data):
    aux_data = np.zeros(image_data.shape)
    factor = 255/image_data.max()
    i = 0
    for row in image_data:
        j = 0
        for element in row:
            if element < 0:
                aux_data[i, j] = 0
            else:
                aux_data[i, j] = int(element*factor)
            j += 1
        i += 1
    filtered_data = aux_data.astype(np.uint8)
    return filtered_data


def fits2py(data):
    """
    Fits files store the image with the origin in the bottom left while python have in the upper left. This function
    transforms fits structure to python.
    :param data: Data from a fits image
    :return: py_data: Data converted to be python readable
    """
    py_data = np.zeros(data.shape)
    for i, row in enumerate(data):
        py_data[data.shape[0] - 1 - i, :] = row
    return py_data
