#!/bin/tcsh
set sportdir = "~arnsaa/shared/Users/dipasa/SPORT"

setenv PYTHONPATH ${PYTHONPATH}:"/home/arnsaa/shared/deps/pip"
cd $sportdir

/home/arnsaa/shared/deps/Python-3.8.0/python ./py/sport-cmd.py ~dipasa/Documents/pipelineProj/data/optics/Athena_par_hyp.spo ~dipasa/Documents/pipelineProj/data/setups/TychoSNR_6keV.sps -rays 100 -out ~dipasa/Documents/pipelineProj/data/outputs/simulation_result -p
