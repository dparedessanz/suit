import os
import json
import sys
import numpy as np
from namespaces import file, fits_header
from namespaces import csv_simulation_dict as sim
from namespaces import spectrum_characteristics as spect
from namespaces import source_characteristics as source_ch
from namespaces import monitor_characteristics as monitor_ch
from namespaces import sport_params
from namespaces import suit_paths
from astropy.io import fits as astrofits
from sim_tools import read_csv, get_file_name, remove_extension, delete_spaces


def main():
    """
    Creates json '.sps' file in order to use for SPORT. This json file will have two set of dictionaries:
        - sources: can be set from the command window or from 'simulation.csv'
        - monitors: by default the focalPlane
    """
    platform_system = sys.platform
    CSV_MODE = False

    i = 1

    while i < len(sys.argv):
        a = sys.argv[i]

        if a == "-h":
            help_create_sps()
            return

        elif a == "-fits":
            fitsFile = sys.argv[i + 1]
            i += 1

        elif a == "-e":
            fits_energy = float(sys.argv[i + 1])
            if fits_energy > 12000 or fits_energy < 100:
                sys.exit("The energy range is between 0.1-12 keV (but in eV).")
            i += 1

        elif a == "-csv":
            CSV_MODE = True
            try:
                csv_path = sys.argv[i + 1]
                i += 1
            except IndexError:
                csv_path = "simulations/simulation.csv"

        else:
            sys.exit("Unrecognized argument %s" % a)

        i += 1

    source_data = []
    monitor_data = []
    if CSV_MODE:
        # Read csv file which must be named 'simulation.csv' I extract fits file name and fits energy for each and
        # append them to source_data
        source_list = getSourcesFromCSV(csv_path)
        obs_object = []
        spc_list = []
        for i, ind_simulation in enumerate(source_list):

            # Check if spectrum is used
            # TODO: create a function to do that
            if ind_simulation[sim.spectrum] != "None":
                use_spectrum = True
            else:
                use_spectrum = False
            spc_list.append(use_spectrum)

            # Create source data
            if ind_simulation[sim.image] is sport_params.point_source:
                if len(source_list) == 1:
                    sim_energy = float(ind_simulation[sim.energy])
                obs_object.append(ind_simulation[sim.image])
                source_data.append(create_source_dict(sport_params.point_source, idSource=i, spFlag=use_spectrum,
                                                      spath=ind_simulation[sim.spectrum],
                                                      energy=ind_simulation[sim.energy],inc=ind_simulation[sim.inc],
                                                      az=ind_simulation[sim.az]))
            else:
                fits_stc = sourcesFromArgs(platform_system, ind_simulation[sim.image], ind_simulation[sim.energy])
                if len(source_list) == 1:
                    sim_energy = float(fits_stc.energy)
                obs_object.append(fits_stc.object)
                source_data.append(create_source_dict(sport_params.extended_source, idSource=i, fpath=fits_stc.globalPath,
                                                      fname=ind_simulation[sim.image], spFlag=use_spectrum,
                                                      spath=ind_simulation[sim.spectrum], fframe=fits_stc.frame,
                                                      flux=fits_stc.avflux, energy=fits_stc.energy,
                                                      inc=ind_simulation[sim.inc], az=ind_simulation[sim.az]))

        # Create file name
        if len(source_list) == 1 and not use_spectrum:
            file_name = obs_object[0] + "_" + str(int(sim_energy / 1000)) + "keV"
        elif check_obs_object(obs_object):
            file_name = obs_object[0]
        else:
            file_name = "different_observations"

    else:
        fits_stc = sourcesFromArgs(platform_system, fitsFile, fits_energy)  # Fits structure created
        file_name = fits_stc.object + "_" + str(int(fits_stc.energy / 1000)) + "keV"
        source_data.append(create_source_dict(sport_params.extended_source, fpath=fits_stc.globalPath, fname=fitsFile,
                                              fframe=fits_stc.frame, flux=fits_stc.avflux, energy=fits_stc.energy))

    monitor_data.append(create_monitor_dict())
    json_data = {
        "sources": source_data,
        "monitors": monitor_data
    }
    only_spectrum = check_energy_modes(spc_list)
    if only_spectrum:
        file_name = file_name + "_spectrum"
        file_path = "./data/setups/" + file_name
    else:
        file_path = "./data/setups/" + file_name

    if not os.path.isfile(file_path + ".sps"):
        with open(file_path + ".sps", "w") as write_json:
            json.dump(json_data, write_json, indent=4)
        with open("simulations/temp/setup_data.txt", "w") as f:
            f.write(suit_paths.working_directory + "/data/setups/" + file_name + ".sps")
    else:
        i = 1
        while os.path.isfile(file_path + "_" + str(i) + ".sps"):
            i += 1

        with open(file_path + "_" + str(i) + ".sps", "w") as write_json:
            json.dump(json_data, write_json, indent=4)
        with open("simulations/temp/setup_data.txt", "w") as f:
            f.write(suit_paths.working_directory + "/data/setups/" + file_name + "_" + str(i) + ".sps")
    return


def create_source_dict(sType, fpath=None, fname=None, spFlag=False, spath=None, idSource="0", inc=0, az=0,
                       energy=1000, flux=1, fframe=0):
    # inc and az in arcmin
    inc = float(inc)*(np.pi/180)/60
    az = float(az)*(np.pi/180)/60
    if sType is sport_params.extended_source and fpath is not None and fname is not None:
        if spFlag:
            sp_dict = create_spectrum_dict(spath)
            source_d = {
                source_ch.type: sType,
                source_ch.id: str(idSource),
                source_ch.inc: str(inc),
                source_ch.az: str(az),
                source_ch.energy: "1",
                source_ch.use_spectrum: True,
                source_ch.flux: flux,
                source_ch.spectrum: sp_dict,
                source_ch.file_path: fpath,
                source_ch.file_name: fname,
                source_ch.frame: fframe
            }
        else:
            source_d = {
                source_ch.type: sType,
                source_ch.id: str(idSource),
                source_ch.inc: str(inc),
                source_ch.az: str(az),
                source_ch.energy: str(energy),
                source_ch.use_spectrum: False,
                source_ch.flux: flux,
                source_ch.file_path: fpath,
                source_ch.file_name: fname,
                source_ch.frame: fframe
            }

    elif sType is sport_params.point_source:
        if spFlag:
            sp_dict = create_spectrum_dict(spath)
            source_d = {
                source_ch.type: sType,
                source_ch.id: str(idSource),
                source_ch.inc: str(inc),
                source_ch.az: str(az),
                source_ch.energy: "1",
                source_ch.use_spectrum: True,
                source_ch.flux: flux,
                source_ch.spectrum: sp_dict
            }
        else:
            source_d = {
                source_ch.type: sType,
                source_ch.id: str(idSource),
                source_ch.inc: str(inc),
                source_ch.az: str(az),
                source_ch.energy: str(energy),
                source_ch.use_spectrum: False,
                source_ch.flux: flux
            }
    else:
        print("Source not created, type, path or file not valid: returning empty dictionary.")
        source_d = {}
    return source_d


def create_monitor_dict(mType=sport_params.focal_monitor, x=["0", "0", "0"], width="1", height="1",
                        monitorName=sport_params.focal_monitor):

    monitor_d = {
        monitor_ch.type: mType,
        monitor_ch.is_round: False,
        monitor_ch.coord_x: x[0],
        monitor_ch.coord_y: x[1],
        monitor_ch.coord_z: x[2],
        monitor_ch.width: width,
        monitor_ch.height: height,
        monitor_ch.name: monitorName
    }
    return monitor_d


def create_spectrum_dict(table_path, e_col=0, e_unit="ev", i_col=1, i_unit="photonflux"):
    spectrum_dict = {
        spect.source_table_path: table_path,
        spect.name_energy_column: e_col,
        spect.name_energy_units: e_unit,
        spect.name_intensity_column: i_col,
        spect.name_intensity_units: i_unit
    }
    return spectrum_dict


def sourcesFromArgs(p_sys, fits_file, fits_energy=1):
    """
    Returns information of the fits file. The frame selected is the first image found in the FITS file given.
    """
    fits_ns = file
    fits_ns.image = remove_extension(fits_file)
    fits_ns.energy = fits_energy
    if p_sys == 'linux' or p_sys == 'linux2':
        fits_ns.globalPath = "/home/dipasa/SUIT/data/observations/" + fits_file  # TODO: substitute for pwd
        fits_ns.workingPath = "./data/observations/" + fits_file
    else:
        fits_ns.globalPath = "../user/save/extendedSources/" + fits_file  # Location of the file in VM
        fits_ns.workingPath = "./data/observations/" + fits_file

    # Reading of FITS file
    try:
        with astrofits.open(fits_ns.workingPath) as file_read:
            frame = 0
            data_found = False
            while not data_found:
                image_data = file_read[frame].data
                is_image = file_read[frame].is_image
                if is_image and image_data is not None:
                    data_found = True
                    fits_ns.frame = frame
                    header = file_read[frame].header
                    fits_ns.object = delete_spaces(header.get(fits_header.object, fits_ns.image))
                    fits_ns.flux = getTotalIntensity(image_data)
                    fits_ns.avflux = getAverageNonZeroIntensity(image_data)
                elif frame == len(file_read):
                    sys.exit("Not image found in file %s" % fits_file)
                else:
                    frame += 1
    except FileNotFoundError:
        sys.exit("File %s not found, please give a valid FITS file." % fits_ns.workingPath)

    return fits_ns


def getSourcesFromCSV(csv_file):
    sim_list = read_csv(csv_file)
    so_list = []
    for simulation in sim_list:
        dict2append = dict()
        if simulation[sim.image] == sport_params.point_source:
            dict2append[sim.image] = sport_params.point_source
        else:
            dict2append[sim.image] = get_file_name(simulation[sim.image])
        dict2append[sim.energy] = simulation[sim.energy]
        dict2append[sim.spectrum] = simulation[sim.spectrum]
        dict2append[sim.inc] = simulation[sim.inc]
        dict2append[sim.az] = simulation[sim.az]
        so_list.append(dict2append)
    return so_list


def check_obs_object(obj_list):
    result = False
    if len(obj_list) > 0:
        result = all(elem == obj_list[0] for elem in obj_list)
    else:
        print("Only one simulation.")
    return result


def check_energy_modes(spectrum_list):
    """
    Check if all simulations are spectrum based or not
    :param spectrum_list: List of use_spectrum booleans (True: spectrum, False: not spectrum)
    :return: Boolean (True: all spectra, False: at least one simulation is not spectrum-based)
    """
    result = all(elem is True for elem in spectrum_list)
    return result


# Functions inherited from SPORT class <class ExtendedSource>
def getFlux(data):
    flux = getAverageNonZeroIntensity(data)
    return flux


def getTotalIntensity(data):
    return float(np.sum(data))


def getAverageNonZeroIntensity(data):
    av_intensity = getTotalIntensity(data)/getNonZeroPixelCount(data)
    return av_intensity


def getNonZeroPixelCount(data):
    return sum([sum([1 for dd in d if dd != 0]) for d in data])


def help_create_sps():
    print("Create '.sps'\n\n")
    print("Available options:")
    print("-fits: takes as argument the fits file, that must be stored in ./data/observations")
    print("-e: takes as argument an energy between 0.1 - 12 keV")
    print("-csv: takes as argument the simulation.csv file")
    return


if __name__ == "__main__":
    main()
