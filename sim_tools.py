import os
import sys
import subprocess
import csv
from shutil import move, rmtree  # Needed for copying the pickle file into the desired directory after its creation
from namespaces import csv_simulation_dict as sim
from set_up import make_file_ex, convert2unix


def run_simulation(plat_sys):
    print('Running simulation...')
    # Create some code to check the version and run update batch file
    if plat_sys == "linux" or plat_sys == "linux2":
        try:
            subprocess.call("./simulations/temp/sim_run.sh")
        except FileNotFoundError:
            sys.exit("There is no executable simulation file.")
    else:
        wor_dir = os.getcwd()
        par_dir = os.path.dirname(wor_dir)
        os.chdir(par_dir + '/SPORT')
        try:
            subprocess.call([par_dir + '/SPORT/sim_run.bat'])
            os.chdir(wor_dir)
        except FileNotFoundError:
            os.chdir(wor_dir)
            sys.exit("There is no executable simulation file.")
    print('Simulation finished.')
    return


def sim_file_creation(plat_sys, o_path, s_path, num_rays=1000, energy=1000, out_name='simulation_result'):
    wor_dir = os.getcwd()
    if plat_sys == 'linux' or plat_sys == 'linux2':
        # It might be needed to run the command 'dos2unix + file_name' in Linux
        sim_file = open("simulations/temp/sim_run.sh", "w")
        sim_file.write("#!/bin/tcsh\n")
        sim_file.write('set sportdir = "~arnsaa/shared/Users/dipasa/SPORT"\n')  # Sport folder location: In thinlinc in Arnee's shared folder
        sim_file.write('setenv PYTHONPATH ${PYTHONPATH}:"/home/arnsaa/shared/deps/pip"\n')
        sim_file.write("cd $sportdir\n\n")
        sim_file.write("/home/arnsaa/shared/deps/Python-3.8.0/python ./py/sport-cmd.py ")
        sim_file.write(o_path + " " + s_path + " ")  # Check that the path is properly selected
        sim_file.write("-rays " + str(num_rays) + " ")
        sim_file.write("-out " + wor_dir + "/data/outputs/" + out_name + " -p\n")
        sim_file.close()

        convert2unix("./simulations/temp/sim_run.sh")
        make_file_ex(plat_sys, "./simulations/temp/sim_run.sh")
    else:  # We are running it on windows, where the SPORT folder must be at the same level of the pipelineProj folder
        # Maybe in the future I can include it inside the pipeline folder
        sim_file = open("../SPORT/sim_run.bat", "w")
        sim_file.write("@echo off\n")
        sim_file.write("vagrant up\n")
        sim_file.write("set DISPLAY=127.0.0.1:0\n")
        sim_file.write("vagrant ssh -- -Y \"cd sport; python3 ./py/sport-cmd.py ")
        sim_file.write(o_path + " " + s_path + " ")
        sim_file.write("-rays " + str(num_rays) + " ")
        sim_file.write("-out ../PipelineProj/data/outputs/" + out_name + " -p\"\n")
        sim_file.write("pause")
        sim_file.close()
    return


def read_csv(csv_file):
    """
    Reads a csv file and return a list of dictionaries
    """
    with open(csv_file, mode='r') as csv_read:
        csv_reader = csv.DictReader(csv_read)
        csv_list = []
        for row in csv_reader:
            csv_list.append(row)
    return csv_list


def write_csv(name, data):
    with open(name, 'w', newline='') as f:
        writer = csv.writer(f)
        for row in data:
            writer.writerow(row)
    return


def split_simulation_list(simulation_list):
    list_of_csv = []
    optics_num = "optic_%d"
    optics_sim = get_unique_simulation_optics(simulation_list)
    for i in range(get_num_optics(simulation_list)):
        data2write = []
        data2write.append(list(simulation_list[0].keys()))
        for simulation in simulation_list:
            if simulation[sim.optics] == optics_sim[i]:
                data2write.append([simulation[sim.optics], simulation[sim.image], simulation[sim.inc],
                                   simulation[sim.az], simulation[sim.energy], simulation[sim.rays]])
        write_csv("temp/" + optics_num % i + "_simulation.csv")
        list_of_csv.append(optics_num % i + "_simulation.csv")
    return list_of_csv


def get_num_optics(simulation_list):
    optics_list = get_simulation_optics(simulation_list)
    num_op = len(set(optics_list))
    return num_op


def get_simulation_optics(simulation_list):
    optics = []
    for simulation in simulation_list:
        optics.append(simulation[sim.optics])
    return optics


def get_unique_simulation_optics(simulation_list):
    optics_list = get_simulation_optics(simulation_list)
    unique_optics = set(optics_list)
    return unique_optics


def get_file_name(file_path):
    folder_delimiter = []
    for i, s in enumerate(file_path):
        if s == "/":
            folder_delimiter.append(i)
    pos = folder_delimiter[len(folder_delimiter) - 1]
    file_name = file_path[(pos+1):len(file_path)]
    return file_name


def move_file(file, from_folder, to_folder):
    move(from_folder + file, to_folder + file)
    return


def remove_extension(file2remove):
    for i, s in enumerate(file2remove):
        if s == ".":
            break
        i += 1
    file_new = file2remove[0:i]
    return file_new


def delete_spaces(string):
    spaces2del = []
    for i, s in enumerate(string):
        if s == " ":
            spaces2del.append(i)

    string_new = string
    tot2del = len(spaces2del)
    for j in range(tot2del):
        pos = spaces2del[tot2del - 1 - j]
        if pos != 0:
            string_new = string_new[0:pos] + string_new[(pos + 1):len(string_new)]
        else:
            string_new = string_new[1:len(string_new)]
    return string_new


def wipe_temp(a=0):
    if a == 0:
        for filename in os.listdir("./simulations/temp/"):
            file_path = os.path.join("./simulations/temp/", filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.remove(file_path)
                elif os.path.isdir(file_path):
                    rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
    elif a == 1:
        os.remove("./simulations/temp/setup_data.txt")
        os.remove("./simulations/temp/sim_run.sh")
        # Must been included as well the auxiliary .csv files for each optic
    elif a == 2:
        os.remove("./simulations/temp/setup_data.txt")
        os.remove("./simulations/temp/sim_run.sh")
    return
