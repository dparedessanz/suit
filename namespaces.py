# Script with different namespaces
from types import SimpleNamespace as ns
import os

fits_header = ns()
fits_header.numAxis = "NAXIS"
fits_header.jAxPixels = "NAXIS%d"  # Length of axis j
fits_header.jAxRefPixel = "CRPIX%d"  # Coordinate of reference pixel in axis j [Rj]
fits_header.linTransPC = "PC%d_%d"  # Linear transformation matrix 'PCi_j formalism' [Mij]
fits_header.linTransCD = "CD%d_%d"  # Linear transformation matrix 'CDi_j formalism' [SiMij]
fits_header.iAxRscToPhys = "CDELT%d"  # Rescale axis i to physical units [Si]
fits_header.iCoordType = "CTYPE%d"  # Coordinate axis type
fits_header.iAxUnits = "CUNIT%d"  # Physical units of axis i
fits_header.iAxRefVal = "CRVAL%d"  # Coordinate value at reference point in axis i
fits_header.paramValues = "PV%d_%d"  # Numeric parameter values [PVi_m]
fits_header.exposure = "EXPOSURE"
fits_header.telescope = "TELESCOP"
fits_header.object = "OBJECT"

suit_paths = ns()
suit_paths.working_directory = os.getcwd()

file = ns()
file.flux = 0
file.energy = 0
file.frame = 0
file.image = ""
file.workingPath = ""
file.globalPath = ""
file.object = ""

suit_analysis = ns()
suit_analysis.plane = "monitor"
suit_analysis.source = "source"
suit_analysis.photon_list = "photonList"
suit_analysis.optics = "optics"

txt_files = ns()
txt_files.simulation_data = "sim_data.txt"
txt_files.setup_info = "./simulations/temp/setup.txt"
txt_files.analysis_results = "results.txt"

csv_files = ns()
csv_files.simulation = "./simulations/simulation.csv"
csv_files.analysis = "./simulations/analysis.csv"
csv_files.optics = "./data/optics/list_of_optics.csv"
csv_files.module_dimensions = './data/optics/module_dimensions.csv'

csv_simulation_dict = ns()
csv_simulation_dict.optics = "OPTICS"
csv_simulation_dict.image = "SOURCE"
csv_simulation_dict.spectrum = "SPECTRUM"
csv_simulation_dict.inc = "INCLINATION"
csv_simulation_dict.az = "AZIMUTH"
csv_simulation_dict.energy = "ENERGY"
csv_simulation_dict.rays = "RAYS"

csv_analysis_dict = ns()
csv_analysis_dict.detector = "DETECTOR"
csv_analysis_dict.fits = "FITS_IMAGE"
csv_analysis_dict.optics = "OPTICS"
csv_analysis_dict.pickle = "PICKLE"

pickle_results = ns()
pickle_results.name = "name"
pickle_results.particles = "particles"
pickle_results.sources = "sources"
pickle_results.optics = "optics"
pickle_results.results = "results"
pickle_results.particle_position = "p"
pickle_results.particle_weight = "w"
pickle_results.particle_trajectory = "col"
pickle_results.source_id = "sourceID"
pickle_results.wavevector = "k"
pickle_results.energy = "e"
pickle_results.mod = "mod"
pickle_results.incidence_vector = "incv"

suit_results = ns()
suit_results.source_name = "SOURCE"
suit_results.energy = "ENERGY"
suit_results.optics_name = "OPTICS"
suit_results.inclination = "INC"
suit_results.azimuth = "AZ"
suit_results.cross_area_simulated = "ACS_SIM"
suit_results.cross_area_analytical = "ACS_AN"
suit_results.effective_area_total = "EA_TOT"
suit_results.effective_area_focused = "EA_FOC"
suit_results.effective_area_straylight = "EA_STR"
suit_results.half_energy_width = "HEW"
suit_results.hew_x = "HEW_X"
suit_results.hew_x_units = "HEW_X_UNIT"
suit_results.hew_y = "HEW_Y"
suit_results.hew_y_units = "HEW_Y_UNIT"

module_characteristics = ns()
module_characteristics.optics = "OPTICS"
module_characteristics.mHeight = "MODULE_HEIGHT"
module_characteristics.pWidth = "PORE_WIDTH"
module_characteristics.pHeight = "PORE_HEIGHT"
module_characteristics.wall = "WALL_THICKNESS"
module_characteristics.plate = "PLATE_THICKNESS"


detector_characteristics = ns()
detector_characteristics.name = "name"
detector_characteristics.type = "type"
detector_characteristics.telescope = "telescope"
detector_characteristics.pixels = "pixels"
detector_characteristics.size = "size"
detector_characteristics.pixel_size = "pixSize"
detector_characteristics.pixel_gap = "pixGap"
detector_characteristics.plate_location = "plateLoc"
detector_characteristics.plate_gap = "plateGap"

source_characteristics = ns()
source_characteristics.type = "type"
source_characteristics.id = "id"
source_characteristics.inc = "inclination"
source_characteristics.az = "azimuth"
source_characteristics.energy = "energy"
source_characteristics.use_spectrum = "useSpectrum"
source_characteristics.spectrum = "spectrum"
source_characteristics.flux = "flux"
source_characteristics.file_path = "filepath"
source_characteristics.file_name = "filename"
source_characteristics.frame = "frame"

spectrum_characteristics = ns()
spectrum_characteristics.source_table_path = "sourceTablePath"
spectrum_characteristics.name_energy_column = "xColumn"
spectrum_characteristics.name_energy_units = "xUnit"
spectrum_characteristics.name_intensity_column = "intensityColumn"
spectrum_characteristics.name_intensity_units = "intensityUnit"

monitor_characteristics = ns()
monitor_characteristics.type = "monitorType"
monitor_characteristics.is_round = "round"
monitor_characteristics.coord_x = "x"
monitor_characteristics.coord_y = "y"
monitor_characteristics.coord_z = "z"
monitor_characteristics.width = "width"
monitor_characteristics.height = "height"
monitor_characteristics.name = "name"

sport_params = ns()
sport_params.point_source = "pointSource"
sport_params.extended_source = "extendedSource"
sport_params.entry_monitor = "entryPlane"
sport_params.focal_monitor = "focalPlane"